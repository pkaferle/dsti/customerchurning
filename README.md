# CustomerChurning

Customer churning prediction from online retail data. 
In this report, I compared different non-parametric classifiers with default and tuned parameters and estimate the most performant one on a given dataset. 
* data cleaning
* feature engineering
* exploratory data analysis (distributions and correlations between the features)
* Modeling: SVM, Random Forest, Gradient Boost, kNN
* Hyperparameter tunning (grid search, randomized search)
* Performance comparison